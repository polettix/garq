Regenerate QR Codes for Google Authenticator files.

This is a [Perl][] program that needs a few modules to work properly. If
this is not immediately clear, there' some help at [Installing Perl
Modules][].

It's possible to test the program using the provided
`example-secret-file`, e.g.:

```
$ ./garq --secret example-secret-file --issuer Whatever
█████████████████████████████████████████
██ ▄▄▄▄▄ █ ▄▄ █▄▄ █ ██▀▄▀▄█ ██▀█ ▄▄▄▄▄ ██
██ █   █ ██▄█▀▀▄█▀▀ ▀█  ▄▀▄ ▀▀▀█ █   █ ██
██ █▄▄▄█ █ ▀▀▄ ▀▀▄ ▄▀█▀ █▄█▄ █ █ █▄▄▄█ ██
██▄▄▄▄▄▄▄█ ▀▄█▄▀▄█▄█ █▄█▄▀ █ █ █▄▄▄▄▄▄▄██
██ ▄▄▀▀ ▄▀  █▄ ▄█▄ ██▀ ▄█ ███▀    ▄▀█  ██
█████ ▀▀▄██▄▄▀ ▄ ▀ ▄▀  ██▄ █  ▀▄ ▀▄ ▄▀▄██
██▀██▄  ▄█ ▄▀██  ███▀ ▄▄█ ██▀▀ █▄ █▄▀▀ ██
██▄   █▄▄▄  ▄▄█▀█▄██▄▄ █▄▄██▄ █▄▄██▄▄▀▄██
████▀ ▀█▄  ▄▀▄ ▄█▄ ▀  █▄█▄ ██  █ ▀██▀▄ ██
███ ▀█▀█▄ ▀▄█▀ ▄ ▀ ▄▀▄▄█▀ █▄▀  ▀▄▀▄▄██▄██
██▀▀▀  █▄█  ███  █▄▄▀█▀█  ██▀▀▄▀ ▀█ ▀  ██
██▄███▄▄▄▄▄█▀▄█▀█ ▄█▀▄▄█ ▄▄▄█▄▀▄█▀▀███▄██
████ ▀█▀▄██▀ ▄ ▄█ ▀▀█▄▀ ██ ▀   █ ▄█ ▀▀ ██
██▄▄▄  ▄▄ █ █▀ ▄ ▄▀▄▄ ▄▄▄▄█▄▀▄  ▄▄▄▀█▀ ██
██▄▄▄▄▄█▄█ █▄██    ██▄  █▄▀▄██ ▄▄▄ ▀██▀██
██ ▄▄▄▄▄ █▀ ▄▄█▀█  ▄  ▀▄  ▄█ ▀ █▄█  ▄▀▄██
██ █   █ ██▄▀▄ ▄█ ▀█▀ █▄█ ██ ▄▄ ▄   ▀ ▄██
██ █▄▄▄█ █ ▀▀▀ ▄ ▄▀▄█▄█▄█▄ ▄▀   ▀█▄▀██▄██
██▄▄▄▄▄▄▄█▄████▄▄▄▄██▄█▄██▄▄██▄█▄█▄███▄██
█████████████████████████████████████████
```

It looks awful in the web page, but in your terminal it should be OK.

That should be everything, apart from...


# COPYRIGHT & LICENSE

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2022 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.


[Perl]: https://www.perl.org/
[Installing Perl Modules]: https://github.polettix.it/ETOOBUSY/2020/01/04/installing-perl-modules/
